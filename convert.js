var getter = document.getElementById("screen");
var screenValue = "";
var temp;
var buttons = document.querySelectorAll("button");

for (var _i = 0, _a = buttons; _i < _a.length; _i++) {
  var item = _a[_i];
  item.addEventListener("click", function (e) {
    var x = e.target;
    var buttonText = x.innerText;
    var screenValue = "";
    if (buttonText == "C") {
      screenValue = "";
      getter.value = screenValue;
    } else if (buttonText == "×") {
      buttonText = "*";
      screenValue += buttonText;
      getter.value += screenValue;
    } else if (buttonText == "÷") {
      buttonText = "/";
      screenValue += buttonText;
      getter.value += screenValue;
    } else if (buttonText == "⌫") {
      screenValue = getter.value.substr(0, getter.value.length - 1);
      getter.value = screenValue;
    } else if (buttonText == "x2") {
      screenValue = getter.value * getter.value;
      getter.value = screenValue;
    } else if (buttonText == "1/x") {
      screenValue = 1 / getter.value;
      getter.value = screenValue;
    } else if (buttonText == "√x") {
      screenValue = Math.sqrt(getter.value);
      getter.value = screenValue;
    } else if (buttonText == "∛x") {
      screenValue = Math.cbrt(getter.value);
      getter.value = screenValue;
    } else if (buttonText == "| x |") {
      screenValue = Math.abs(getter.value);
      getter.value = screenValue;
    } else if (buttonText == "xy") {
      buttonText = "**";
      screenValue += buttonText;
      getter.value += screenValue;
    } else if (buttonText == "兀") {
      buttonText = "3.14";
      screenValue += buttonText;
      getter.value += screenValue;
    } else if (buttonText == "𝑒") {
      buttonText = "2.718";
      screenValue += buttonText;
      getter.value += screenValue;
    } else if (buttonText == "10x") {
      screenValue = Math.pow(10, getter.value);
      getter.value = screenValue;
    } else if (buttonText == "mod") {
      buttonText = "%";
      screenValue += buttonText;
      getter.value += screenValue;
    } else if (
      buttonText == "Trigonometry" ||
      buttonText == "expand_more" ||
      buttonText == "Functions" ||
      buttonText == " expand_more"
    ) {
      getter.value = "";
    } else if (buttonText == "n!") {
      var n = getter.value;
      var x_1 = 1;
      var i = void 0;
      for (i = 2; i <= n; i++) x_1 = x_1 * i;
      screenValue = x_1;
      getter.value = screenValue;
    } else if (buttonText == "exp") {
      screenValue = Math.exp(screenValue).toPrecision(6);
      getter.value = screenValue;
    } else if (buttonText == "log") {
      screenValue = Math.log10(getter.value);
      getter.value = screenValue;
    } else if (buttonText == "ln") {
      screenValue = Math.log(getter.value);
      getter.value = screenValue;
    } else if (buttonText == "DEG") {
      screenValue = (180 * getter.value) / Math.PI;
      getter.value = screenValue.toPrecision(6);
    } else if (buttonText == "RAD") {
      screenValue = (Math.PI * getter.value) / 180;
      getter.value = screenValue.toPrecision(6);
    } else if (buttonText == "=") {
      screenValue = eval(getter.value);
      getter.value = screenValue;
    } else if (buttonText == "MS") {
      temp = parseInt(getter.value);
    } else if (buttonText == "M+") {
      temp = temp + parseInt(getter.value);
    } else if (buttonText == "M-") {
      temp = temp - parseInt(getter.value);
    } else if (buttonText == "MC") {
      temp = "";
      getter.value = temp;
    } else if (buttonText == "MR") {
      screenValue = temp;
      getter.value = screenValue;
    } else {
      screenValue += buttonText;
      getter.value += screenValue;
    }
  });
}
var trigno = function (tf) {
  var x = document.getElementById("screen");
  var screenValue = x.innerText;
  if (tf == "sine") {
    screenValue = String(Math.sin(getter.value));
    getter.value = screenValue;
  }
  if (tf == "cos") {
    screenValue = Math.cos(getter.value).toString();
    getter.value = screenValue;
  }
  if (tf == "tan") {
    screenValue = Math.tan(getter.value).toString();
    getter.value = screenValue;
  }
  if (tf == "cot") {
    screenValue = (1 / Math.tan(getter.value)).toString();
    getter.value = screenValue;
  }
  if (tf == "sec") {
    screenValue = (1 / Math.cos(getter.value)).toString();
    getter.value = screenValue;
  }
  if (tf == "cosec") {
    screenValue = (1 / Math.sin(getter.value)).toString();
    getter.value = screenValue;
  }
};
var func = function (get) {
  var x = document.getElementById("screen");
  var screenValue = x.innerText;
  if (get == "ceil") {
    screenValue = Math.ceil(getter.value).toString();
    getter.value = screenValue;
  }
  if (get == "floor") {
    screenValue = Math.floor(getter.value).toString();
    getter.value = screenValue;
  }
  if (get == "round") {
    screenValue = Math.round(getter.value).toString();
    getter.value = screenValue;
  }
};
